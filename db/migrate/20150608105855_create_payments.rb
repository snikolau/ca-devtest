class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.boolean :success, default: false
      t.integer :user_tab_id
      t.decimal :value, precision: 8, scale: 2
      t.datetime :paid_at
      t.timestamps
    end
    add_index :payments, :user_tab_id
  end
end
