class CreateUserTabs < ActiveRecord::Migration
  def change
    create_table :user_tabs do |t|
      t.integer :user_id
      t.integer :tab_id

      t.timestamps
    end
    add_index :user_tabs, :user_id
    add_index :user_tabs, :tab_id
  end
end
