class CreateTabs < ActiveRecord::Migration
  def change
    create_table :tabs do |t|
      t.integer :venue_id
      t.timestamps
    end
    add_index :tabs, :venue_id
  end
end
