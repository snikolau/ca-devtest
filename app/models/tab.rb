# == Schema Information
#
# Table name: tabs
#
#  id         :integer          not null, primary key
#  venue_id   :integer
#  created_at :datetime
#  updated_at :datetime
#

class Tab < ActiveRecord::Base
  belongs_to :venue
  has_many :user_tabs
end
