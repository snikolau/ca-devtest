# == Schema Information
#
# Table name: user_tabs
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  tab_id     :integer
#  created_at :datetime
#  updated_at :datetime
#

class UserTab < ActiveRecord::Base
  belongs_to :user
  belongs_to :tab

  has_many :payments
end
