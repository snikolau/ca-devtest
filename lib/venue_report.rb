class VenueRow < Struct.new(:name, :transactions, :value)
  def transactions
    super.to_i
  end

  def value
    super.to_f.round(2)
  end
end

class VenueReport
  def initialize(from, to = Date.today)
    @from = date(from)
    @to = date(to)
  end

  # All logic were pushed to DB and is executed as 1 request (there is 1 subquery).
  # This optimisation required using of manual SQL code.
  def result
    results = []
    query_1 = ::Tab.joins(user_tabs: [:payments])
              .where('payments.success = ? AND payments.paid_at BETWEEN ? AND ?', true, @from, @to)
              .select('tabs.id as ttab_id, tabs.venue_id, user_tabs.id as tuser_tab_id, payments.user_tab_id,
                      COALESCE(SUM(payments.value), 0) AS venue_sum,
                      COALESCE(COUNT(payments.id), 0) AS transaction_count')
              .group('tabs.venue_id')

    # Cannot use find_each there, as it resets ordering to order('id ASC')
    ::Venue.joins("LEFT JOIN (#{query_1.to_sql}) as ttabs on venues.id = ttabs.venue_id")
      .select('venues.id, venues.name, ttabs.venue_sum, ttabs.venue_id, ttabs.transaction_count')
      .order('ttabs.venue_sum DESC').each do |r|
      results << VenueRow.new(r.name, r.transaction_count, r.venue_sum)
    end

    results
  end

  private

  def date(date)
    case date
      when Date
        date
      when Time
        date.to_date
      when String
        begin
          Date.parse(date)
        rescue
          raise new ArgumentError, "Invalid date format #{date}"
        end
      else
        fail new ArgumentError, "Invalid date format #{date}"
    end
  end
end
