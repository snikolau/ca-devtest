# == Schema Information
#
# Table name: payments
#
#  id          :integer          not null, primary key
#  success     :boolean          default(FALSE)
#  user_tab_id :integer
#  value       :decimal(8, 2)
#  paid_at     :datetime
#  created_at  :datetime
#  updated_at  :datetime
#

FactoryGirl.define do
  factory :payment do
  end
end
