require 'venue_report'
require 'rails_helper'

RSpec.describe VenueReport do
  # Constructs test data
  # @param set [Array]
  #   Structure: [venue object, payment value, payment success, paid_at date]
  def create_entry(set)
    tab = create(:tab, venue: set[0])
    user_tab = create(:user_tab, tab: tab)
    create(:payment, value: set[1], success: set[2], paid_at: set[3], user_tab: user_tab)
  end

  let(:v1) { create(:venue, name: 'Test 1') }
  let(:v2) { create(:venue, name: 'Test 2') }
  let(:v3) { create(:venue, name: 'Test 3') }

  context 'when user provides time range' do
    it 'returns right data' do
      data = [
        [v1, 5, true, 1.year.ago],
        [v1, 1, true, 8.months.ago],
        [v1, 1, false, 8.months.ago],
        [v2, 3, false, 12.months.ago],
        [v3, 4, true, 5.months.ago]
      ]

      data.each do |set|
        create_entry(set)
      end

      report = VenueReport.new(Time.now)
      result = report.result
      expect(result.size).to eq 3

      report = VenueReport.new(5.months.ago)
      result = report.result
      expect(result[0].name).to eq 'Test 3'
      expect(result[0].transactions).to eq 1
      expect(result[0].value).to eq 4
      expect(result[1].value).to eq 0

      report = VenueReport.new(12.months.ago)
      result = report.result
      expect(result[0].name).to eq 'Test 1'
      expect(result[0].transactions).to eq 2
      expect(result[0].value).to eq 6
      expect(result[1].name).to eq 'Test 3'
      expect(result[1].value).to eq 4
      expect(result[1].transactions).to eq 1
      expect(result[2].value).to eq 0
      expect(result[2].transactions).to eq 0
      expect(result[2].name).to eq 'Test 2'
    end
  end
end
